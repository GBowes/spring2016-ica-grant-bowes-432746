create table pets (
id int unsigned not null auto_increment,
species ENUM ('cat','dog','fish','bird','hamster') not null default 'cat',
name varchar(50) not null,
filename varchar(150) not null, weight decimal(4,2) not null,
description tinytext,
primary key (id)
)engine = innodb default character set = utf8 collate = utf8_general_ci;