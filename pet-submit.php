<!DOCTYPE html>
<head><title>Pet Submit</title></head>
    <body>
        <?php
            
            if (!isEmpty($_POST['species']) || (!isEmpty($_POST['name'])) || (!isEmpty($_POST['weight'])) || (!isEmpty($_POST['description'])) || (!isEmpty($_POST['picture']))):
                echo "error: did not fill in all forms"
                exit();
            endif;
            
            $species =  $_POST['species'];
            $name =  $_POST['name'];
            $weight = (float) $_POST['weight'];
            $description = $_POST['description'];
            $picture = $_POST['picture'];

            $stmt = $mysqli->prepare("insert into pets (name, species, weight, description, picture) values (?,?,?,?,?)");
            if(!$stmt){
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
            }
             
            $stmt->bind_param('sss', $name, $species, $weight, $description, $picture);
             
            $stmt->execute();
             
            $stmt->close();
        ?>
    </body>
</html>